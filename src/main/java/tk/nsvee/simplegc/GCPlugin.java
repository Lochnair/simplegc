package tk.nsvee.simplegc;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class GCPlugin extends JavaPlugin {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {
		if (cmd.getName().equalsIgnoreCase("gc")) {
			long before = Runtime.getRuntime().freeMemory();
			System.gc();
			long after = Runtime.getRuntime().freeMemory();
			float freed = (after - before) / 1024 / 1024;
			sender.sendMessage("Freed " + (freed > 0 ? freed : 0) + " MB memory");
			return true;
		} else
		if (cmd.getName().equalsIgnoreCase("mem")) {
			long free = Runtime.getRuntime().freeMemory() / 1024 / 1024;
			long max = Runtime.getRuntime().maxMemory() / 1024 / 1024;
			long used = max - free;
			String msg = String.format("RAM: %d / %d (Free: %d)", used, max, free);
			sender.sendMessage(msg);
			return true;
		}
		
		return false;
	}
	
	@Override
	public void onEnable() {
		System.gc();
	}
}
